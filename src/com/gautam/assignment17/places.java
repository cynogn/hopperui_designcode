package com.gautam.assignment17;

import java.util.HashMap;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

/*
 * places activity for adding new places 
 */
public class places extends Activity {
	ImageView img;
	int CAMERA_PIC_REQUEST = 2;
	int TAKE_PICTURE = 0;
	Boolean flag = false;
	Uri mCapturedImageURI = null;
	Spinner spinner = null;;
	EditText mUsernameEditText;
	EditText mAddressEditText;
	EditText mDescriptionEditText;
	Button b1;
	Bitmap photo;
	String selectedItem = null;

	Button mAddPlaceButton;
	Button mCancelButton;
	HashMap<String, String> hm = new HashMap<String, String>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_places);

		mAddPlaceButton = (Button) findViewById(R.id.button21);
		mCancelButton = (Button) findViewById(R.id.button22);
		mUsernameEditText = (EditText) findViewById(R.id.editText11);
		mAddressEditText = (EditText) findViewById(R.id.editText22);
		mDescriptionEditText = (EditText) findViewById(R.id.editText33);
		img = (ImageView) findViewById(R.id.imageView1);
		b1 = (Button) findViewById(R.id.DASHBOARD);

		if (customTitleSupported) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
					R.layout.addplacetitle1);
		}

		mAddPlaceButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				/*
				 * Error validation
				 */
				if (!flag)

				{
					Toast.makeText(places.this, "Plese take your image",
							Toast.LENGTH_SHORT).show();
				}
				if(flag)
				{
					if (mUsernameEditText.toString().equals("")
							|| mAddressEditText.toString().equals("")
							|| mAddressEditText.toString().equals("")) {
						Toast.makeText(places.this, "Plese Enter your details",
								Toast.LENGTH_SHORT).show();
					} else {
						selectedItem = spinner.getSelectedItem().toString();
						Intent i111 = new Intent(places.this, addPlaces.class);
						hm.put(mUsernameEditText.getText().toString()
								.toLowerCase(), spinner.getSelectedItem()
								.toString()
								+ ","
								+ mAddressEditText.getText().toString()
										.toLowerCase()
								+ ","
								+ mDescriptionEditText.getText().toString()
										.toLowerCase());

						Bundle b1 = new Bundle();
						b1.putString("v1", (spinner.getSelectedItem()
								.toString()
								+ ","
								+ mAddressEditText.getText().toString()
										.toLowerCase()
								+ ","
								+ mDescriptionEditText.getText().toString()
										.toLowerCase() + "").toLowerCase());

						b1.putString("img1", mCapturedImageURI.toString());
						b1.putParcelable("pic", photo);

						i111.putExtras(b1);
						startActivity(i111);
						finish();
					}
				}

			}
		});
		/*
		 * Cancel button closes and moves to the rpevious activity
		 */
		mCancelButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				finish();
			}
		});

		/*
		 * Setting the content of the spinner
		 */
		spinner = (Spinner) this.findViewById(R.id.spinner1);

		ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(this,
				android.R.layout.test_list_item, new String[] { "Pub",
						"Restaurent", "School", "Others" });

		spinner.setAdapter(spinnerArrayAdapter);

		img.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				try {
					String filemUsernameEditText = "temp.jpg";
					ContentValues values = new ContentValues();
					values.put(MediaStore.Images.Media.TITLE,
							filemUsernameEditText);
					mCapturedImageURI = getContentResolver().insert(
							MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
							values);
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					intent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
					startActivityForResult(intent, CAMERA_PIC_REQUEST);
				} catch (Exception ee) {
					Toast.makeText(places.this, "Image not taken",
							Toast.LENGTH_SHORT);
					img.setImageResource(R.drawable.friends_noimg);

				}
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try {
			if (requestCode == CAMERA_PIC_REQUEST) {

				photo = (Bitmap) data.getExtras().get("data");
				img.setImageBitmap(photo);
				flag = true;

			} else {
			}
		} catch (Exception ee) {
			Toast.makeText(places.this, "Image not taken", Toast.LENGTH_SHORT);
			img.setImageResource(R.drawable.friends_noimg);

		}

	}
}
