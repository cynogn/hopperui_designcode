package com.gautam.assignment17;


/**
 * Profile contains the information about that person
 * 
 * @author Gautam B Version 1
 * 
 */
public class profile extends Activity {
	TextView mName;
	TextView mMobileNumber;
	TextView mGender;
	TextView mDob;
	TextView mCity;
	TextView mQualification;

	Button mDashboardTitleButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

		setContentView(R.layout.activity_profile);

		mName = (TextView) findViewById(R.id.value1);
		mMobileNumber = (TextView) findViewById(R.id.value2);
		mGender = (TextView) findViewById(R.id.value3);
		mDob = (TextView) findViewById(R.id.value4);
		mCity = (TextView) findViewById(R.id.value5);
		mQualification = (TextView) findViewById(R.id.value7);
		/*
		 * Hard coded the values of the profile
		 */
		mName.setText("Gautam");
		mMobileNumber.setText("8867288663");
		mGender.setText("Female");
		mDob.setText("25.07.1990");
		mCity.setText("Coimbatore");
		mQualification.setText("B.Tech in Computer Science");

		// Setting the the Custom Title bar profile.xml

		if (customTitleSupported) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
					R.layout.profile);
		}
		mDashboardTitleButton = (Button) findViewById(R.id.DASHBOARD);

		mDashboardTitleButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent i1 = new Intent(profile.this, Dashboard.class);
				Bundle b1 = new Bundle();
				b1.putString("v1", "000");
				i1.putExtras(b1);
				startActivity(i1);
				finish();
			}
		});

	}
}
