package com.gautam.assignment17;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Add places displays the added places
 * 
 * @author Gautam
 * 
 */
public class addPlaces extends ListActivity {
	Button mDashboardButton;
	TextView mAddPlacesTitle;
	Button mAddPlacesButton;
	ListView mListView;
	String[] SplitValues1;
	Intent i2;

	public void onListItemClick(ListView parent, View v, int position, long id) {
		Toast.makeText(addPlaces.this, "position is" + position,
				Toast.LENGTH_LONG).show();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

		super.onCreate(savedInstanceState);
		i2 = getIntent();
		setContentView(R.layout.main);

		if (customTitleSupported) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
					R.layout.sample);
		}
		mAddPlacesButton = (Button) findViewById(R.id.ADDBUTTON);

		mAddPlacesTitle = (TextView) findViewById(R.id.TITLE);
		mAddPlacesTitle.setText("Add Places");
		mAddPlacesTitle.setTextColor(Color.WHITE);
		String mValue = i2.getStringExtra("v1");
		if (mValue.toString().equals("000")) {
		} else {
			Toast.makeText(addPlaces.this, "" + i2.getStringExtra("v1"),
					Toast.LENGTH_SHORT).show();
			Toast.makeText(addPlaces.this, "" + i2.getStringExtra("v1"),
					Toast.LENGTH_SHORT).show();
String SplitValues = i2.getStringExtra("v1").toString();
SplitValues1 = i2.getStringExtra("v1").toString().split(",");

			setListAdapter(new MyAdapter(this,new String[]{SplitValues}));
		}

		mAddPlacesButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent i1 = new Intent(addPlaces.this, places.class);
				startActivity(i1);
				finish();

			}
		});
		mDashboardButton = (Button) findViewById(R.id.DASHBOARD);
		mDashboardButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent i1 = new Intent(addPlaces.this, Dashboard.class);
				Bundle mDashboardButton = new Bundle();
				mDashboardButton.putString("v1", "000");
				i1.putExtras(mDashboardButton);
				startActivity(i1);
				finish();
			}
		});

	}

	private class MyAdapter extends ArrayAdapter<String> {

		public MyAdapter(Context c ,
				String[] stringArray) {

			super(c, 1, stringArray);

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			LayoutInflater in = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View r = in.inflate(R.layout.list_item, parent, false);
			String[] items = getResources().getStringArray(R.array.numbers);
			ImageView i = (ImageView) r.findViewById(R.id.imageView1);
			TextView t1 = (TextView) r.findViewById(R.id.vText1);
			TextView t2 = (TextView) r.findViewById(R.id.vText2);
			TextView t3 = (TextView) r.findViewById(R.id.vText3);
			t1.setText(SplitValues1[0]);
			t2.setText(SplitValues1[1]);
			t3.setText(SplitValues1[2]);
			
			i.setImageResource(R.drawable.add_photo);
			Bitmap bMap = BitmapFactory.decodeFile(i2.getStringExtra("img1")
					+ "");
			Bitmap bp = i2.getParcelableExtra("pic");
			i.setImageBitmap(bp);

			return r;
		}

	}
}
