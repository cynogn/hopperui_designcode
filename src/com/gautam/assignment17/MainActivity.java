package com.gautam.assignment17;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Gautam MainActivity displays the login screen
 * 
 */
public class MainActivity extends Activity implements OnClickListener {

	Button b1;
	EditText username;
	EditText password;
	Button login;
	TextView register;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);

		setContentView(R.layout.login_design);

		username = (EditText) findViewById(R.id.loginUsername);
		password = (EditText) findViewById(R.id.loginPassword);
		login = (Button) findViewById(R.id.loginButton);
		register = (TextView) findViewById(R.id.signupButton);
		username.setBackgroundResource(R.drawable.txt_field);
		password.setBackgroundResource(R.drawable.txt_field);
		login.setOnClickListener(this);
		register.setOnClickListener(this);

		if (customTitleSupported) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
					R.layout.itle_page);
		}
	}

	/*
	 * switching to Register / Signup page
	 */
	public void onClick(View v) {
		if (v.getId() == register.getId()) {
			Intent i11 = new Intent(MainActivity.this, Register_Activity.class);
			startActivity(i11);
			finish();
		}
		/*
		 * Error vaidtaion
		 */
		if (v.getId() == login.getId()) {
			if (username.getText().toString().equals("")
					|| password.getText().toString().equals("")) {
				username.setBackgroundResource(R.drawable.txt_fielderror);
				password.setBackgroundResource(R.drawable.txt_fielderror);

			}

			else {
				Intent i111 = new Intent(MainActivity.this, Dashboard.class);
				Bundle b1 = new Bundle();
				b1.putString("v1", "000");
				i111.putExtras(b1);
				startActivity(i111);
				finish();

			}
		}
	}
}