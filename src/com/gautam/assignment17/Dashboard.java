package com.gautam.assignment17;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * Dashboard activity
 * @author Gautam
 * 
 */
public class Dashboard extends Activity {
	Button mPlaces;
	Button mProfile;
	String val = "000";
	Intent i;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);

		mPlaces = (Button) findViewById(R.id.buttonPlaces);
		mPlaces.setOnClickListener(new OnClickListener() {
			/*
			 * moving to app places activity
			 */

			public void onClick(View v) {
				Intent i123 = new Intent(Dashboard.this, addPlaces.class);
				Bundle b23 = new Bundle();
				b23.putString("v1", "000");
				b23.putString("img1", "000");
				i123.putExtras(b23);
				startActivity(i123);
				finish();
			}

		});
	mProfile = (Button) findViewById(R.id.button2);
		
		
		 
		mProfile.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent i1 = new Intent(Dashboard.this, profile.class);
				startActivityForResult(i1, 1);
				finish();
			}
		});
	}

}
