package com.gautam.assignment17;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * User can signup for login in the Register Class
 * 
 * @author Gautam
 * 
 * 
 * 
 */
public class Register_Activity extends Activity {
	EditText mEmail;
	EditText mUname;
	EditText mPass;
	Button mRegister;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_register_);

		mEmail = (EditText) findViewById(R.id.editText1);
		mUname = (EditText) findViewById(R.id.editText2);
		mPass = (EditText) findViewById(R.id.editText3);
		mRegister = (Button) findViewById(R.id.signup);
		mEmail.setBackgroundResource(R.drawable.txt_field);
		mUname.setBackgroundResource(R.drawable.txt_field);
		mPass.setBackgroundResource(R.drawable.txt_field);

		mRegister.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				if (mEmail.getText().toString().equals("")
						|| mUname.getText().toString().equals("")
						|| mPass.getText().toString().equals("")) {

					Toast.makeText(Register_Activity.this,
							"Mask sure all data is entered", Toast.LENGTH_SHORT)
							.show();
					/*
					 * Setting the Background on invalid entry
					 */
					mEmail.setBackgroundResource(R.drawable.txt_fielderror);
					mUname.setBackgroundResource(R.drawable.txt_fielderror);
					mPass.setBackgroundResource(R.drawable.txt_fielderror);

				} else {

					Intent i1 = new Intent(Register_Activity.this,
							Dashboard.class);
					Bundle b1 = new Bundle();
					b1.putString("v1", "000");
					i1.putExtras(b1);
					startActivity(i1);
					finish();
				}

			}
		});
		/*
		 * Setting up the action bar for navigation
		 */
		if (customTitleSupported) {
			getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
					R.layout.register);
		}
	}
}
